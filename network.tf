resource "digitalocean_firewall" "all" {
  name = "all"

  droplet_ids = [digitalocean_droplet.app.id, digitalocean_droplet.db.id]

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = var.ssh_ips
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "all"
    source_droplet_ids = [digitalocean_droplet.app.id, digitalocean_droplet.db.id]
  }

  inbound_rule {
    protocol         = "icmp"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "all"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "all"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}

resource "digitalocean_firewall" "web" {
  name = "only-8084"

  droplet_ids = [digitalocean_droplet.app.id]

  inbound_rule {
    protocol         = "tcp"
    port_range       = "8084"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }
}

