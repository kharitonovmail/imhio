resource "digitalocean_droplet" "app" {
    image = "centos-7-x64"
    name = "app"
    region = "fra1"
    size = "s-1vcpu-1gb"
    private_networking = true

    ssh_keys = [
        "${digitalocean_ssh_key.default.fingerprint}"
    ]
}
resource "digitalocean_droplet" "db" {
    image = "centos-7-x64"
    name = "db"
    region = "fra1"
    size = "s-1vcpu-1gb"
    private_networking = true

    ssh_keys = [
        "${digitalocean_ssh_key.default.fingerprint}"
    ]
    user_data = <<-EOF
                  #cloud-config
                  mounts:
                  - [ /dev/disk/by-id/scsi-0DO_Volume_data, /var/lib/mysql, "xfs", "defaults", "0", "0" ]
                  EOF
}
