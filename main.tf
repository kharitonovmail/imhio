resource "null_resource" "ansible" {
  provisioner "local-exec" {
    command = <<EOT
       cat vault.yml >> variables.yml
       sleep 60 && ansible-playbook main.yml -i inventory.yml -u root --vault-password-file ${var.pass} --ssh-common-args '-o StrictHostKeyChecking=no'
       sleep 20 && curl -X POST ${digitalocean_droplet.app.ipv4_address}:8084/v1/health/check
    EOT
  }
  depends_on = [
    local_file.variables_file,
    digitalocean_firewall.all,
    digitalocean_volume_attachment.db
  ]
}
