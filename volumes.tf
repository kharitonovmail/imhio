resource "digitalocean_volume" "db-data" {
  region                  = "fra1"
  name                    = "data"
  size                    = 1
  initial_filesystem_type = "xfs"
  description             = "database volume"
}
resource "digitalocean_volume_attachment" "db" {
  droplet_id = digitalocean_droplet.db.id
  volume_id  = digitalocean_volume.db-data.id
}
