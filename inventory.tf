data "template_file" "data_inventory" {
  depends_on = [
    digitalocean_droplet.app,
    digitalocean_droplet.db
  ]
  template = "${file("./inventory.tpl")}"

  vars = {
    app_ip = join("\n", digitalocean_droplet.app.*.ipv4_address)
    db_ip = join("\n", digitalocean_droplet.db.*.ipv4_address)
 }
}

resource "local_file" "inventory_file" {
  content  = data.template_file.data_inventory.rendered
  filename = "inventory.yml"
}

resource "local_file" variables_file {

  filename = "variables.yml"
  content = <<EOF
PKG_URL: ${var.pkg_url}
DB_IP_PRIV: ${digitalocean_droplet.db.ipv4_address_private}
APP_IP: ${digitalocean_droplet.app.ipv4_address}
APP_IP_PRIV: ${digitalocean_droplet.app.ipv4_address_private}
DB_USER: ${var.db_user}
DB_NAME: ${var.db_name}
EOF
  depends_on = [
    local_file.inventory_file,
  ]

}
