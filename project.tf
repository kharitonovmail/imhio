resource "digitalocean_project" "test" {
  name        = var.projectname
  description = "A project to represent development resources."
  purpose     = "Web Application"
  environment = "Development"
  resources   = [digitalocean_droplet.app.urn, digitalocean_droplet.db.urn]
}
