variable "projectname" {}

variable "public_key" {}

variable "pkg_url" {}

variable "db_name" {}

variable "db_user" {}

variable "pass" {}

variable "do_token" {}

provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_ssh_key" "default" {
  name       = "ssh key"
  public_key = file("${var.public_key}")
}

